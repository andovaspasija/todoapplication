export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const EDIT_TODO = 'EDIT_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const DELETE_ALL_TODO_ITEMS = 'DELETE_ALL_TODO_ITEMS';
export const SET_ACTIVE_FILTER = 'SET_ACTIVE_FILTER';


export const addToDoAction = todo => ({
    type: ADD_TODO,
    payload: todo,
});

export const toggleToDoAction = todo => ({
    type: TOGGLE_TODO,
    payload: todo,
});

export const deleteToDoAction = todo => ({
    type: DELETE_TODO,
    payload: todo,
});

export const editToDoAction = todo => ({
    type: EDIT_TODO,
    payload: todo,
});

export const updateToDoAction = todo => ({
    type: UPDATE_TODO,
    payload: todo,
});

export const deleteAllToDoItemsAction = filter => ({
    type: DELETE_ALL_TODO_ITEMS,
});

export const setActiveFilterAction = filter => ({
    type: SET_ACTIVE_FILTER,
    payload: filter,
});


