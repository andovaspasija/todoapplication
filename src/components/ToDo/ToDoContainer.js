import React, {Component} from 'react';
import {connect} from 'react-redux';
import './ToDoStyle.css';
import ToDoList from './ToDoList';
import ToDoFilter from './ToDoFilter';
import ToDoHeader from './ToDoHeader';
import {
    addToDo,
    editToDo,
    toggleToDo,
    updateToDo,
    deleteToDo,
    deleteAllToDoItems,
    setActiveFilter
} from "./ToDoActions";

class ToDoContainer extends Component {
    render() {
        return (
            <div className='todo-container'>
                <ToDoHeader {...this.props}/>
                <ToDoList {...this.props}/>
                <ToDoFilter {...this.props}/>
            </div>
        );
    }
}



const getToDoItems = (todos, filter) => {
    if (filter === "active") {
        return todos.filter(todo => (!todo.completed));
    } else if (filter === "completed") {
        return todos.filter(todo => (todo.completed));
    } else if (filter === "all") {
        return todos;
    }
};

const mapStateToProps = state => ({
    todos: getToDoItems(state.toDoReducer.todos, state.toDoReducer.filter),
    filter: state.toDoReducer.filter,
});

const mapDispatchToProps = {
    addToDo,
    editToDo,
    toggleToDo,
    updateToDo,
    deleteToDo,
    deleteAllToDoItems,
    setActiveFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoContainer);
