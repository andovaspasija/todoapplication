import React from 'react';
import PropTypes from 'prop-types';


export default function ToDoList(props) {
    return (
        <div className='todo-list'>
            {props.todos.map(todo => {
                if (todo.edit) {
                    return <EditToDoItem todo={todo}{...props}/>;
                } else {
                    return <ToDoItem todo={todo}{...props}/>;
                }
            })}
        </div>
    );
};

ToDoList.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired,
            edit: PropTypes.bool.isRequired,
            completed: PropTypes.bool.isRequired
        })
    ).isRequired,
    filter: PropTypes.oneOf(['all', 'active', 'completed']).isRequired,
};

function ToDoItem({todo, editToDo, toggleToDo, deleteToDo}) {
    return (
        <div className='todo-item'>
            {!todo.completed ?
                <span>
                    <i className='fa fa-edit todo-icon' onClick={(event) => editToDo(todo, event)}/>
                </span> : ''
            }
            <span className={todo.completed ? 'todo-item-completed' : 'todo-item-normal'}
                  onClick={(event) => toggleToDo(todo, event)}>{todo.text}
            </span>
            <span>
                <i className='fa fa-remove todo-icon' onClick={(event) => deleteToDo(todo, event)}/>
            </span>
        </div>
    );
}

function EditToDoItem({todo, updateToDo}) {
    return (
        <div className='edit-todo-item'>
            <form onSubmit={(event) => updateToDo(todo, event)}>
                <input className='edit-todo-input' placeholder={todo.text} autoFocus/>
                <button className='edit-todo-button'>
                    <i className='fa fa-check todo-icon'/>
                </button>
            </form>
        </div>
    );
}

