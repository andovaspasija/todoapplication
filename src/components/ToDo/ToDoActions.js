import {
    addToDoAction,
    toggleToDoAction,
    deleteToDoAction,
    editToDoAction,
    updateToDoAction,
    deleteAllToDoItemsAction,
    setActiveFilterAction,
} from './ToDoActionCreators';


export function addToDo(text) {
    return dispatch => {
        dispatch(addToDoAction({text, id: Date.now(), completed: false, edit: false}));
    }
}

export function toggleToDo(todo) {
    return dispatch => {
        dispatch(toggleToDoAction(todo));
    }
}

export function deleteToDo(todo) {
    return dispatch => {
        dispatch(deleteToDoAction(todo));
    }
}

export function editToDo(todo) {
    return dispatch => {
        dispatch(editToDoAction(todo));
    }
}

export function updateToDo(todo, event) {
    event.preventDefault();
    let newValue = event.currentTarget.firstChild.value;
    return dispatch => {
        dispatch(updateToDoAction({...todo, text: newValue.trim() !== '' ? newValue : todo.text, edit : false}));
    }
}

export function deleteAllToDoItems(todo) {
    return dispatch => {
        dispatch(deleteAllToDoItemsAction(todo));
    }
}

export function setActiveFilter(event) {
    return dispatch => {
        dispatch(setActiveFilterAction({filter: event.currentTarget.value}));
    }
}




