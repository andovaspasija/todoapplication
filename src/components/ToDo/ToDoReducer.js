const initialState = {todos: [], filter: 'all'};

const actionHandlers = {
    ADD_TODO: addToDo,
    EDIT_TODO: editToDo,
    TOGGLE_TODO: toggleToDo,
    UPDATE_TODO: updateToDo,
    DELETE_TODO: deleteToDo,
    DELETE_ALL_TODO_ITEMS: deleteAllToDoItems,
    SET_ACTIVE_FILTER: setActiveFilter,
};

export default function toDoReducer(state = initialState, action) {
    let actionHandler = actionHandlers[action.type];
    if (actionHandler) {
        return actionHandler(state, action);
    }
    return state;
}

function addToDo(state, action) {
    state.todos.forEach(todo => {
        todo.edit = false;
    });
    return {...state, todos: [action.payload, ...state.todos ]};
}

function editToDo(state, action) {
    let todos = state.todos.map(todo => (todo.id === action.payload.id) ? {...todo, edit: true} : {
        ...todo,
        edit: false
    });
    return {...state, todos: todos};
}

function toggleToDo(state, action) {
    return {
        ...state,
        todos: state.todos.map(todo => (todo.id === action.payload.id) ? {
            ...todo,
            completed: !action.payload.completed
        } : todo)
    };
}

function updateToDo(state, action) {
    return {...state, todos: state.todos.map(todo => (todo.id === action.payload.id) ? action.payload : todo)};
}

function deleteToDo(state, action) {
    return {...state, todos: state.todos.filter(todo => (todo.id !== action.payload.id))};
}

function deleteAllToDoItems(state, action) {
    return initialState;
}

function setActiveFilter(state, action) {
    return {...state, filter: action.payload.filter};
}

