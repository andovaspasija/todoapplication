import React from 'react';

export default function ToDoFilter({filter, setActiveFilter, deleteAllToDoItems}) {
    return (
        <span className='todo-filter'>
            <button value="all" disabled={filter === 'all'} onClick={setActiveFilter}>All</button>
            <button value="active" disabled={filter === 'active'} onClick={setActiveFilter}>Active</button>
            <button value="completed" disabled={filter === 'completed'}
                    onClick={setActiveFilter}>Completed</button>
            <button onClick={deleteAllToDoItems}><i className='fa fa-trash'/>  Clear all</button>
        </span>
    );
}