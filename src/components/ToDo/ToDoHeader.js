import React, {Component} from 'react';

export default class ToDoHeader extends Component {
    state = {
        todo: ''
    };

    handleChange = (event) => {
        this.setState({
            todo: event.target.value
        });
    };

    onSubmit = (event) => {
        event.preventDefault();
        if (this.state.todo.trim() !== '') {
            this.props.addToDo(this.state.todo);
            this.setState({
                todo: ''
            });
        }
    };

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input className='todo-header' placeholder="What's on your mind ?" value={this.state.todo}
                       onChange={(event) => this.handleChange(event)}/>
            </form>
        );
    }
}