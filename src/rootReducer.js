import {combineReducers} from 'redux';
import toDoReducer from './components/ToDo/ToDoReducer';

export const rootReducer = combineReducers({toDoReducer});

export default rootReducer;
