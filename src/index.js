import React from 'react';
import ReactDOM from 'react-dom';
import {composeWithDevTools} from 'redux-devtools-extension';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import rootReducer from './rootReducer';
import App from './app';

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(
            thunk,
        ),
    ),
);

ReactDOM.render(
    <Provider store={{...store}}>
        <App/>
    </Provider>,
    document.getElementById('root'),
);
