# ToDoApplication
This is a small hello-world to-do react-redux application.

## Features
- Add new todo item
- Delete todo
- Edit todo
- Mark todo as completed on click on the item
- Delete all todos at once
- Filter todos by: all, active and completed

## Tech
- [node.js]
- [npm]
- [Javascript]
- [JSX]
- [React]
- [Redux]
- [CSS]

## Installation
###### Prerequisites
- Node.js and npm installed

###### If node.js and npm are not installed follow this step:
- Download NVM for Windows from the following link https://github.com/coreybutler/nvm-windows/releases (Download nvm-setup.zip)
- From terminal run: nvm install 14.17.5  (14.17.5 is the latest node.js version at this point) -> this will install npm 6.14.14 automatically and set all needed environment variables
- From terminal run: nvm use 14.17.5 

## Development local start
- Locate into folder where package.json file is and open terminal 
- From terminal run: npm install
- From terminal run: npm run start
-> Local server should be attached on port 3000


# Playground:
[https://todoapplication-70c9e.web.app/]


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [node.js]: <https://nodejs.org/en/>
   [npm]: <https://www.npmjs.com/>
   [Javascript]: <https://developer.mozilla.org/en-US/>
   [JSX]: <https://reactjs.org/docs/introducing-jsx.html>
   [React]: <https://reactjs.org/>
   [Redux]: <https://redux.js.org/>
   [node.js]: <http://nodejs.org>
   [CSS]: <https://developer.mozilla.org/en-US/docs/Web/CSS>
   [https://todoapplication-70c9e.web.app/]: <https://todoapplication-70c9e.web.app/>